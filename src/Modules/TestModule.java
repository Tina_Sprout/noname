package Modules;

import Networking.Rest.Discord;
import Objects.Discord.Channel;
import Objects.Execution;
import Objects.Module.Command;
import Objects.Module.Module;
import Objects.Module.Settings.CommandSetting;
import Objects.Module.Settings.ModuleSetting;
import Utils.EmbedHelper;
import Utils.Managers.*;
import Utils.WebsocketPayloadHelper;
import com.sun.xml.internal.ws.wsdl.writer.document.Message;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.HashMap;

@Module.DefineModule(Identifier = "DebugModule")
public class TestModule {

    @Module.DefineCommand(Identifier = "TestCommand1", Triggers = "P:test1")
    public void TestCommand1(Execution execparams) {
        System.out.println(Discord.CreateMessage(execparams.message.channelID, EmbedHelper.GenerateMaxEmbed()));
    }

    @Module.DefineCommand(Identifier = "TestCommand2", Triggers = "P:test2")
    public void TestCommand2(Execution execution, TestCommand2Settings settings) {
        if(execution.message.getContent().contains(" ")) {
            Channel channel = execution.message.getChannel();
            TestCommand2Settings dddd = new TestCommand2Settings();
            dddd.W = execution.message.getContent().split(" ")[1];

            if(!channel.ModuleOverrides.containsKey("DebugModule")) channel.ModuleOverrides.put("DebugModule", new ModuleSetting());
            channel.ModuleOverrides.get("DebugModule").Commands.put("TestCommand2", dddd);
            ChannelManager.UpdateChannel(channel, execution.message);
        } else {///
            Channel channel = execution.message.getChannel();
            System.out.println(((TestCommand2Settings)channel.ModuleOverrides.get("DebugModule").Commands.get("TestCommand2")).W);
            execution.message.Reply(((TestCommand2Settings)channel.ModuleOverrides.get("DebugModule").Commands.get("TestCommand2")).W);
        }
    }



}
