package Utils.Serializers;

import Utils.Managers.GuildManager;
import Objects.Discord.Channel;
import Objects.Discord.Guild;
import Objects.Module.Settings.ModuleSetting;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.ResolvableDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;

public class ChannelDeserializer extends StdDeserializer<Channel> implements ResolvableDeserializer{
    private final JsonDeserializer<?> defaultSerializer;
    public ChannelDeserializer(JsonDeserializer<?> defaultser) {
        super(Channel.class);
        this.defaultSerializer = defaultser;
    }
    //When i made this only god and I knew what i was doing.
    //Now only god knows
    @Override
    public Channel deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Channel refchan = (Channel) this.defaultSerializer.deserialize(jsonParser,deserializationContext);
        if(refchan.ModuleOverrides != null && !refchan.ModuleOverrides.isEmpty() && refchan.GuildSnowflake != null) {
            HashMap<String, ModuleSetting> overrides = refchan.ModuleOverrides;
            refchan.ModuleOverrides = new HashMap<>();
            Guild guild = GuildManager.GetGuild(refchan.GuildSnowflake);
            overrides.keySet().forEach(T->{
                if(guild.ModuleSettings.containsKey(T)) {
                    ModuleSetting tooveride = guild.ModuleSettings.get(T);
                    for (Field field : overrides.get(T).getClass().getDeclaredFields()) {
                        field.setAccessible(true);
                        if(field.isAnnotationPresent(JsonProperty.class)) {
                            try {
                                Field overidingfield = tooveride.getClass().getDeclaredField(field.getName());
                                overidingfield.setAccessible(true);
                                overidingfield.set(tooveride, field.get(overrides.get(T)));
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    refchan.ModuleOverrides.put(T,tooveride);
                }
            });
        }
        return refchan;
    }

    @Override
    public void resolve(DeserializationContext deserializationContext) throws JsonMappingException {

    }
}
