package Utils.Serializers;

import Objects.Discord.Channel;
import Objects.Discord.Guild;
import Objects.Module.Settings.ModuleSetting;
import Utils.Managers.GuildManager;
import Utils.Managers.ModuleManager;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

public class ChannelDeserializerNew extends StdDeserializer<HashMap<String, ModuleSetting>>{
    protected ChannelDeserializerNew(Class<?> vc) {
        super(vc);
    }

    public ChannelDeserializerNew() {
        this(null);
    }

    @Override
    public HashMap deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        HashMap<String, ModuleSetting> ret = new HashMap<>();
        JsonNode jsonNode = jsonParser.getCodec().readTree(jsonParser);
        Channel channel = (Channel) jsonParser.getCurrentValue();
        if(channel.type == Channel.ChannelType.GUILD_TEXT.ordinal()) {
            Guild guild = GuildManager.GetGuild(channel.GuildSnowflake);
            ObjectMapper objectMapper = new ObjectMapper();
            jsonNode.fieldNames().forEachRemaining(FIELDNAME->{
                if(!guild.ModuleSettings.containsKey(FIELDNAME)) {
                    System.out.println(FIELDNAME+":"+jsonNode.get(FIELDNAME));
                    jsonNode.get(FIELDNAME).fieldNames().forEachRemaining(KEY->{
                        System.out.println(KEY+":----"+jsonNode.get(FIELDNAME).get(KEY));

                    });
                        System.out.println(FIELDNAME);
                    try {
                   //     objectMapper.readValue(jsonNode.get(FIELDNAME).toString(), ModuleSetting.class);
                        //todo this was changed from tostirng to tobindaryvalue
                                  ret.put(FIELDNAME, objectMapper.readValue(jsonNode.get(FIELDNAME).binaryValue() , ModuleSetting.class));
                } catch (IOException e) {
                    e.printStackTrace();
                    }
                }
            });
        }
        return ret;
    }
}
