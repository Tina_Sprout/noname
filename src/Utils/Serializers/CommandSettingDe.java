package Utils.Serializers;

import Objects.Module.Command;
import Objects.Module.Settings.CommandSetting;
import Utils.Managers.ModuleManager;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.HashMap;

public class CommandSettingDe extends StdDeserializer<HashMap<String,CommandSetting>>{
    protected CommandSettingDe(Class<?> vc) {
        super(vc);
    }
public CommandSettingDe(){this(null);}
    @Override
    public HashMap<String,CommandSetting> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        HashMap<String, CommandSetting> commands = new HashMap<>();
        JsonNode jsonNode = jsonParser.getCodec().readTree(jsonParser);
        jsonNode.fieldNames().forEachRemaining(KEY->{
            System.out.println(KEY);
            Command command = ModuleManager.GetCommandByIdentifier(KEY);
            if(!(command == null)){
                try {
                    System.out.println(command.PREFCMD);
                    commands.put(KEY, objectMapper.readValue(jsonNode.get(KEY).toString(), command.PREFCMD));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        return commands;

  }
}
