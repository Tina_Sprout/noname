package Utils.Serializers;

import Modules.TestCommand2Settings;
import Objects.Discord.Channel;
import Objects.Discord.Guild;
import Objects.Module.Settings.ModuleSetting;
import Utils.Managers.GuildManager;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

public class channelserialixernew extends StdSerializer<HashMap<String, ModuleSetting>>{
    protected channelserialixernew(Class<HashMap<String, ModuleSetting>> t) {
        super(t);
    }
    public channelserialixernew(){this (null);}
    @Override
    public void serialize(HashMap<String, ModuleSetting> chanmap, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        Channel channel = (Channel) jsonGenerator.getCurrentValue();
        jsonGenerator.writeStartObject();


        if(channel.type == Channel.ChannelType.GUILD_TEXT.ordinal()) {
            if(chanmap.isEmpty()){
                
                jsonGenerator.writeEndObject();
                return;
            }
            ObjectMapper objectMapper = new ObjectMapper();
            //todo uncomment this as its commented out just for testing
//            Guild guild = GuildManager.GetGuild(channel.GuildSnowflake);
            //todo remove this cus its just used for testing
            Guild guild = GenerateDemoGuild();

            if(guild.ModuleSettings.isEmpty()) {
                    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);
                    chanmap.keySet().forEach(T->{
                        try {
                            JSONObject serialized = new JSONObject(objectMapper.writeValueAsString(chanmap.get(T)));

                            //test for empties
                            CheckJson(serialized);
                            final boolean[] written = {false};
                            serialized.keySet().forEach(L->{
                                try {
                                    if(!written[0]){
                                        jsonGenerator.writeObjectFieldStart(T);
                                        written[0] = true;
                                    }
                    //todo: this actually needs to be redone :c
                                           jsonGenerator.writeFieldName(L);

//                                    jsonGenerator.writeRawValue(serialized.get(L).toString());

//                                    jsonGenerator.writeRaw("{\""+L+"\":"+String.valueOf(serialized.get(L)+"}"));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            });
                            if(written[0]){
                                jsonGenerator.writeEndObject();
                                written[0] = false;
                            }

                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                    objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
            }  else {
                //todo make some diff stuff here using json objects and also use checkjson for cleanup oe empty
                chanmap.keySet().forEach(KEY->{
                    if(!guild.ModuleSettings.containsKey(KEY)) {
                        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);
                        try {
                            JSONObject serialized = new JSONObject(objectMapper.writeValueAsString(chanmap.get(KEY)));
                            CheckJson(serialized);
                            final boolean[] B = {false};
                            serialized.keySet().forEach(L->{
                                if(B[0]==false){
                                    try {
                                        jsonGenerator.writeFieldName(KEY);
                                        B[0] = true;
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                }
                                try {
                                    System.out.println("SEWRIALIZING"+L);
                                    jsonGenerator.writeRaw(String.format("{\"%s\":%s}",L,serialized.get(L)));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            });

                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                        objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
                    } else {

                        try {
                            JSONObject serialized = new JSONObject(objectMapper.writeValueAsString(chanmap.get(KEY)));
                            serialized = CompareJson(serialized, new JSONObject(objectMapper.writeValueAsString(guild.ModuleSettings.get(KEY))));
                            serialized = CheckJson(serialized);
                            if(!serialized.toString().equalsIgnoreCase("{}")) {
                                jsonGenerator.writeFieldName(KEY);
                                jsonGenerator.writeRawValue(serialized.toString());
                            }
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        }
        jsonGenerator.writeEndObject();
    }

    private JSONObject CompareJson(JSONObject serialized, JSONObject jsonObject) {
        JSONObject ret = new JSONObject();
        serialized.keys().forEachRemaining(T->{
            
            String samplefromchan = serialized.get(T).toString();
            if(!jsonObject.has(T)){
                
                ret.put(T,serialized.get(T));
            } else {
                String samplefromguild = jsonObject.get(T).toString();
                
                if(samplefromchan.startsWith("{")&& samplefromchan.endsWith("}")) {
                    
                    ret.put(T, CompareJson(new JSONObject(samplefromchan), new JSONObject(samplefromguild)));
                } else {
                    
                    if(!samplefromchan.equals(samplefromguild)){
                        ret.put(T,samplefromchan);
                    }
                }
                //todo add arrays support
            }
        });
        return ret;
    }

    private JSONObject CheckJson(JSONObject serialized) {
        
        serialized.keys().forEachRemaining(KEY->{
            String toclean = serialized.get(KEY).toString();
            if(toclean.equalsIgnoreCase("{}")) {
                
                serialized.remove(KEY);
            }
            else if(toclean.startsWith("{") && toclean.endsWith("}") && toclean.contains("\":")) {
                serialized.put(KEY, CheckJson(serialized.getJSONObject(KEY)));
                if(serialized.getJSONObject(KEY).toString().equalsIgnoreCase("{}")) {
                    serialized.remove(KEY);
                }
            }
        });
        
        return serialized;
    }

    private Guild GenerateDemoGuild() {
        Guild guild = new Guild();
        guild.Snowflake = "2891938120380913";
        ModuleSetting setting = new ModuleSetting();
        setting.Commands.put("command1", new TestCommand2Settings());
        guild.ModuleSettings.put("module2",setting);
        guild.ModuleSettings.get("module2").Commands.get("command1").enabled=false;
        return guild;
    }
}
