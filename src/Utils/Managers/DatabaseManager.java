package Utils.Managers;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

import java.util.Arrays;

public class DatabaseManager {
    private static MongoClient client;
    private static DB  Database;
    public static Jongo jongo;
    public static void init(String arg, String arg1) {
        MongoCredential credential = MongoCredential.createCredential(arg, "admin", arg1.toCharArray());
        client = new MongoClient(new ServerAddress("discordapp.tools",27017), Arrays.asList(credential));
        Database = client.getDB("NONAME2");
        jongo = new Jongo(Database);
    }

    public static MongoCollection getCollection(String name) {

        return jongo.getCollection(name);
    }
}
