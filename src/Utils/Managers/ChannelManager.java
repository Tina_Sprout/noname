package Utils.Managers;

import Modules.TestCommand2Settings;
import Networking.Rest.Discord;
import Objects.Discord.Channel;
import Objects.Discord.Message;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jongo.MongoCollection;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

public class ChannelManager {
    static MongoCollection ChannelCollection;
    static HashMap<String, Channel> Channels = new HashMap<>();

    public static void init() {
        ChannelCollection = DatabaseManager.getCollection("channels");
    }
    public static Channel GetChannel(String channelID) {
        System.out.println("getting");
        if(Channels.containsKey(channelID))
        return Channels.get(channelID);
        Channel channel = ChannelCollection.findOne(String.format("{_id:\'%s\'}",channelID)).as(Channel.class);
        if(channel != null) {
            Channels.put(channelID,channel);
            return channel;
        } else {
            try {
                System.out.println("gettin from discord :c");
                 channel = objectMapper.readValue(Discord.GetChannel(channelID).toString().replaceAll("\"id\":\"", "\"_id\":\""), Channel.class);
                Channels.put(channelID, channel);
                ChannelCollection.insert(channel);
                return channel;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    static ObjectMapper objectMapper = new ObjectMapper();
    public static void OnMessageCreated(JSONObject d) {
        if (!d.getJSONObject("author").has("bot")) {
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);
            try {
                Message message = objectMapper.readValue(d.toString(), Message.class);
                    ModuleManager.ProcessMessage(message);
                objectMapper.writeValue(System.out, message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public static void UpdateChannel(Channel channel, Message message) {

        //ChannelCollection.insert(channel);
        try {
            System.out.println("wirting output value..");
            new ObjectMapper().writeValue(System.out, channel);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("writing what jongo doe1");
        System.out.println(DatabaseManager.jongo.getMapper().getMarshaller().marshall(channel));
        message.Reply(DatabaseManager.jongo.getMapper().getMarshaller().marshall(channel).toString());
        ChannelCollection.update(String.format("{_id:'%s'}",channel.Snowflake)).with(channel);
    }
}
