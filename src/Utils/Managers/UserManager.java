package Utils.Managers;

import Objects.Discord.User;

import java.util.HashMap;

public class UserManager {
    static HashMap<String, User> UsersInMemory = new HashMap<>();

    public static User GetUser(String authorID) {
        return UsersInMemory.get(authorID);
    }
}
