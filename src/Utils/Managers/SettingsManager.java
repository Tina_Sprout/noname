package Utils.Managers;

import Objects.Discord.Channel;
import Objects.Discord.Guild;
import Objects.Discord.Message;
import Objects.Module.Settings.ModuleSetting;

public class SettingsManager {
    public static void UpdateGuildSetting(Guild guild, ModuleSetting moduleSetting, String ModuleName) {
            guild.ModuleSettings.put(ModuleName, moduleSetting);
        GuildManager.UpdateGuild(guild);
    }


    public static void UpdateChannelSetting(Channel channel, ModuleSetting moduleSetting, String modulename) {
            channel.ModuleOverrides.put(modulename,moduleSetting);
        ChannelManager.UpdateChannel(channel, new Message());
    }
}
