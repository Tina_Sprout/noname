package Utils.Managers;

import Networking.Sockets.GatewaySocket;
import Networking.Sockets.VoiceEventSocket;
import Utils.WebsocketPayloadHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class SocketManager {
    public static ArrayList<GatewaySocket> GatewayShards = new ArrayList<>();
    private static HashMap<String, VoiceEventSocket> VoiceEventSockets = new HashMap<>();
    public static String GatewayURL = "wss://gateway.discord.gg";

    public static void AddShard() {
        try {
            GatewayShards.add(new GatewaySocket(GatewayShards.size()));
            GatewayShards.get(GatewayShards.size()-1).Connect();
            Thread.sleep(600);
            GatewayShards.get(0).Send(3, WebsocketPayloadHelper.precense(WebsocketPayloadHelper.PrecenseType.playing, "hdsda"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
