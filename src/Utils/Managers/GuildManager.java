package Utils.Managers;

import Networking.Rest.Discord;
import Objects.Discord.Guild;
import org.jongo.MongoCollection;

import java.io.IOException;
import java.util.HashMap;

public class GuildManager {
    static MongoCollection GuildsCollection;
    static HashMap<String, Guild> Guilds = new HashMap<>();
    public static Guild GetGuild(String guildSnowflake) {
        if(Guilds.containsKey(guildSnowflake))
        return Guilds.get(guildSnowflake);
        Guild guild = GuildsCollection.findOne(String.format("{_id:\'%s\'}",guildSnowflake)).as(Guild.class);
        if(guild == null)
        try {
                     guild =  ChannelManager.objectMapper.readValue(Discord.GetGuild(guildSnowflake).toString().replaceAll("\"id\":\"","\"_id\":\""), Guild.class);
            GuildsCollection.insert(guild);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(!Guilds.containsKey(guildSnowflake))
        Guilds.put(guildSnowflake,guild);
        return guild;
    }
    public static void init() {
        GuildsCollection = DatabaseManager.getCollection("guilds");
    }

    public static void UpdateGuild(Guild guild) {
        Guilds.put(guild.Snowflake, guild);
        GuildsCollection.update(String.format("{_id:\'%s\'}",guild.Snowflake),guild);
    }
}
