package Utils.Managers;

import Objects.Discord.Channel;
import Objects.Discord.Message;
import Objects.Module.Command;
import Objects.Module.Module;
import Objects.Module.Settings.ModuleSetting;
import org.reflections.Reflections;

import java.util.HashMap;

public class ModuleManager {
    public static HashMap<String, Module> Modules = new HashMap<>();

    //todo: if channeloveries has module return that, else if guild has modulesettings reutrn that, else return defult
    public static ModuleSetting GetSettingsForModule(String identifier, Channel channel) {
        if(channel.ModuleOverrides.containsKey(identifier)) return channel.ModuleOverrides.get(identifier);
        else if (GuildManager.GetGuild(channel.GuildSnowflake).ModuleSettings.containsKey(identifier)) return GuildManager.GetGuild(channel.GuildSnowflake).ModuleSettings.get(identifier);
        else return new ModuleSetting();
    }




    public static void ProcessMessage(Message message) {
        //todo: change this prefix to channel/guild spesific
        if(message.getContent().startsWith("#")) {
                new Thread(()->{
                    for (Module module : Modules.values()) {
                           module.ProcessCMDTriggers(message);
                    }
                }).start();
        }
    }


    public static void init() {
        new Reflections("Modules").getTypesAnnotatedWith(Module.DefineModule.class).forEach(T->{
            try {
                Module module = new Module(T);
                Modules.put(module.getIdentifier(), module);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        });
    }



    public static Command GetCommandByIdentifier(String s) {
        for (Module module : Modules.values())  {
            if (module.Commands.containsKey(s))
                return module.Commands.get(s);
        }
        return null;

    }

    public static Module GetModuleForCommand(Command s) {
        for (Module module: Modules.values()) {
            if (module.Commands.containsValue(s))
                return module;
        }
        return null;
    }
}
