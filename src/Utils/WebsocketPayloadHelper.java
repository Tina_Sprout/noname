package Utils;

import org.json.JSONObject;

public class WebsocketPayloadHelper {
    public enum PrecenseType{playing,streaming,listening,watching}
    public static JSONObject precense(PrecenseType type, String name) {
        return precense(type,name, null);
    }
    public static JSONObject precense(PrecenseType type, String name, String url) {
        return new JSONObject().put("game", new JSONObject(){{
            put("type",type.ordinal());
            put("name",name);
            
                 if(url!=null) put("url",url);
        }}).
        put("afk", false).
        put("since", JSONObject.NULL).
        put("status", "online");

    }
}
