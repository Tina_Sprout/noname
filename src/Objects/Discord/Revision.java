package Objects.Discord;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Revision {
    @JsonProperty("content") String content;
    //@JsonProperty("embeds")
    @JsonProperty("time")
    public String timestamp;
    @JsonProperty(value = "deleted", required = false) boolean Deleted = false;
    @JsonProperty(value = "attachments", required = false) public String[] Attachments = {};
}
