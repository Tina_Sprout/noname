package Objects.Discord;

import Objects.Module.Settings.ModuleSetting;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Guild {
    @JsonProperty(value = "ModuleSettings", required = false) public HashMap<String, ModuleSetting> ModuleSettings = new HashMap<>();
    @JsonProperty(value = "_id", required = true)
    public String Snowflake;
    @JsonProperty(value = "name",required=false) String Name;
    @JsonProperty(value = "icon",required=false) String IconURL;
    @JsonProperty(value = "splash",required=false) String Splash;
    @JsonProperty(value = "owner_id",required=false) String OwnerID;
    @JsonProperty(value = "region",required=false) String Region;
    @JsonProperty(value = "afk_channel_id	",required=false) String AFKCHANID;
    @JsonProperty(value = "afk_timeout	integer	",required=false) int AFKTimeout;
    //@JsonProperty(value = "roles",required=false) Role[] Roles;
    //@JsonProperty(value = "emojis",required=false); //prob gonna do somthing else for emotes
    /*@JsonProperty(value = "mfa_level	",required=false);
    @JsonProperty(value = "application_id",required=false);
    @JsonProperty(value = "widget_channel_id",required=false);
    @JsonProperty(value = "joined_at",required=false);
    @JsonProperty(value = "large",required=false);
    @JsonProperty(value = "unavailable",required=false);
    @JsonProperty(value = "member_count",required=false);
    @JsonProperty(value = "voice_states",required=false);
    @JsonProperty(value = "members",required=false);*/
    @JsonProperty(value = "channels",required=false) String Channels[];

}
