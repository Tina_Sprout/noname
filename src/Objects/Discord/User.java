package Objects.Discord;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;

public class User {
    enum AccountType{Banned,Kicked,Bot,Normal,Premium,GlobalMod,GlobalAdmin,Senpai}
    @JsonProperty("_id") String Snowflake;
    @JsonProperty("username") String Username;
    @JsonProperty("disciminator") String Discriminator;
    @JsonProperty("avatar") String Avatar;
    @JsonProperty("accounttype") AccountType Accounttype;
    @JsonProperty("email") String email;
    //Extra
    @JsonProperty("color") String Color;
    @JsonProperty("background") String Background;
    @JsonProperty("token") String token;
    @JsonProperty("experience") HashMap<String, Integer> EXPs;

}
