package Objects.Discord;


import Objects.Module.Settings.ModuleSetting;
import Utils.Serializers.ChannelDeserializerNew;
import Utils.Serializers.channelserialixernew;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.HashMap;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Channel {

    //Discord spesific
    @JsonProperty(value = "_id",required = true)
    public String Snowflake;
    @JsonProperty(value = "type", required = true)
    public int type;
    @JsonProperty(value = "guild_id",required = false) public String GuildSnowflake;
    @JsonProperty(value = "name", required = false) String Name;
    @JsonProperty(value = "topic", required = false) String Topic;
    @JsonProperty(value = "position", required = false) int Position;
    @JsonProperty(value = "nsfw", required = false) boolean NSFW;
    @JsonProperty(value = "user_limit", required = false) int UserLimit;
    @JsonProperty(value = "owner_id", required = false) String OwnerID;
    @JsonProperty(value = "parent_id", required = false) String CategoryID;
    public enum ChannelType{GUILD_TEXT,DM,GUILD_VOICE,GROUP_DM,GUILD_CATEGORU}
    public Channel(){}
    //None discord.
    //todo: make a custom marsaler and marsaler for module overides
    @JsonProperty(value = "ModuleOverrides", required = false)
    @JsonDeserialize(using = ChannelDeserializerNew.class) //to get the right types
    @JsonSerialize(using = channelserialixernew.class) //to check for dupes so its actually overides
    public HashMap<String, ModuleSetting> ModuleOverrides = new HashMap<>();
}
