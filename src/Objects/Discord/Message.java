package Objects.Discord;

import Networking.Rest.Discord;
import Utils.Managers.ChannelManager;
import Utils.Managers.UserManager;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Message {
    @JsonProperty("channel_id")
    public String channelID;
    @JsonProperty("_id") String Snowflake;
    @JsonProperty("author_id") String AuthorID;
    @JsonProperty("revisions") ArrayList<Revision>Revisions = new ArrayList<Revision>();
    @JsonProperty(value = "mention_everyone", required = false) boolean mentioneveryone = false;
    //@JsonProperty(value = "pinned", required = false) boolean Pinned = false;
    //@JsonProperty(value = "type", required = false) int msgtype;

    @JsonProperty(value = "mention_roles", required = false) String[] rolementions = {};

    @JsonIgnore
    public Channel getChannel() {
        return ChannelManager.GetChannel(channelID);
    }
    @JsonIgnore
    public String getContent() {
        return getCurrentRevision().content;
    }
    @JsonIgnore
    public Revision getCurrentRevision() {
        return Revisions.get(Revisions.size()-1);
    }
    @JsonIgnore
    public User getAuthor() {
        return UserManager.GetUser(AuthorID);
    }

    @JsonProperty(value = "content", access = JsonProperty.Access.WRITE_ONLY) //Just for discords websocket
    public void CreateRevision(String content) {
        if(Revisions.size() == 0) Revisions.add(new Revision());
        Revisions.get(Revisions.size()-1).content = content;
    }

    @JsonProperty(value = "attachments", access = JsonProperty.Access.WRITE_ONLY)
    public void AddAttachments(String[] ats) {
        if(Revisions.size() == 0) Revisions.add(new Revision());
        Revisions.get(Revisions.size()-1).Attachments = ats;
    }
    @JsonProperty(value = "timestamp", access = JsonProperty.Access.WRITE_ONLY)
    public void AddTimeStamp(String stamp) {
        if(Revisions.size() == 0) Revisions.add(new Revision());
        Revisions.get(Revisions.size()-1).timestamp = stamp;
    }

    @JsonProperty(value = "author", access = JsonProperty.Access.WRITE_ONLY)
    public void SetAutherID (Map<String, String> obj) {
        this.AuthorID = obj.get("_id");
    }

    public void Reply(String msg) {
        //QUICK REPLY
        System.out.println(Discord.CreateMessage(channelID, msg));
    }
}
