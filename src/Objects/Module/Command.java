package Objects.Module;

import Objects.Discord.Channel;
import Objects.Discord.Message;
import Objects.Execution;
import Objects.Module.Settings.CommandSetting;
import Objects.Module.Settings.ModuleSetting;
import Utils.Managers.ModuleManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class Command {


    public Class<? extends CommandSetting> PREFCMD;
    public Method method;
    private String[] Identifiers;
    public Command(Method body, Module.DefineCommand definition) {
        System.out.println("making cmd");
        this.method = body;
        Identifiers = definition.Identifier();
        PREFCMD = this.method.getParameterCount() == 1 ? CommandSetting.class : CommandSetting.class.isAssignableFrom(this.method.getParameterTypes()[1]) ? (Class<? extends CommandSetting>) this.method.getParameterTypes()[1] : CommandSetting.class;
        for (int x = 0; x < definition.Triggers().length;x++) {
            String s = definition.Triggers()[x];
            Trigger trigger = new Trigger();
            trigger.Triggertype = Trigger.TriggerType.valueOf(s.split(":")[0]);
            trigger.value = s.split(":")[1].toLowerCase();
            if(!Triggers.containsKey(trigger.Triggertype))
            Triggers.put(trigger.Triggertype, new ArrayList<>());
            Triggers.get(trigger.Triggertype).add(trigger);
        }
    }
    //todo: it would prob be better to have indivudual hashmaps with the key as the value for the trigger.
    HashMap<Trigger.TriggerType, ArrayList<Trigger>> Triggers = new HashMap<>();



    public void Execute(Message message, Module module, Trigger trigger) {
        Execution execution = new Execution(message, trigger);
        try {
            if(method.getParameterCount() == 1)
            method.invoke(module.Instance, execution);
            else if(method.getParameterCount() == 2)
                method.invoke(module.Instance, execution, PREFCMD.cast(GetSettings(PREFCMD,message.getChannel(),module)));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public <T extends CommandSetting> T GetSettings(Class<? extends CommandSetting> T, Channel channel, Module module) {
        ModuleSetting moduleSetting = ModuleManager.GetSettingsForModule(module.getIdentifier(),channel);
        if(moduleSetting.Commands.containsKey(GetIdentifier())) {
            System.out.println("found set");
            return (T) (moduleSetting.Commands.get(GetIdentifier()));
        }
        else try {
            System.out.println("made new");
            return (T) PREFCMD.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println("fuck null set");
        return null;
    }

    public String GetIdentifier() {
        return Identifiers[0];
    }

}
