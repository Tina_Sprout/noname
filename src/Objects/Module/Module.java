package Objects.Module;

import Objects.Discord.Message;
import Objects.Events.Event;
import Objects.Module.Settings.CommandSetting;
import Objects.Module.Settings.ModuleSetting;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;
import java.util.HashMap;

import static Objects.Module.Module.PermissionLevel.Normal;
import static Objects.Module.Trigger.TriggerType.P;

public class Module {


    private String identifier;
    private String[] information;
    public HashMap<String, Command> Commands = new HashMap<>();
    public String getIdentifier() {
        return identifier;
    }

    public void ProcessCMDTriggers(Message message) {
        Commands.values().forEach(T->{
            if (T.Triggers.containsKey(P))
            T.Triggers.get(P).forEach(Y->{
                Trigger L = Y;
                System.out.println("tesitng");
                if(L.isTriggerd(message)){
                    T.Execute(message, this, L);
                }
            });
        });
    }

    enum PermissionLevel{Normal,Member,Premium,Mod,Admin,Owner,Senpai}
    @Retention(RetentionPolicy.RUNTIME)
    public @interface DefineModule {
        String Identifier();
        String[] Information() default {"Short help not provided","Medium side help not provided, but it should contain like syntax info and shit", "HARDCORE FULL OUT INFO NOT PRDOVIDEDIts"};
    }

    @Retention(RetentionPolicy.RUNTIME)
    public @interface DefineCommand {
        String[] Identifier();
        String[] Triggers();
        PermissionLevel MinLevel() default Normal;
    }
    @Retention(RetentionPolicy.RUNTIME)
    public @interface DefineTopic {
        String[] Identifier();
    }
    @Retention(RetentionPolicy.RUNTIME)
    public @interface DefineLisener {
        String Identifier();
        Class<? extends Event> ListeningTo();
    }

    Object Instance;
    Class ModuleClass;

    public Module(Class<?> modules) throws IllegalAccessException, InstantiationException {
        System.out.println("making");
        ModuleClass = modules;
        Instance = modules.newInstance();
        DefineMe();
        DefineCommands();
    }

    private void DefineCommands() {
        for (Method method : ModuleClass.getMethods()) {
            System.out.println("mak");
            if(method.isAnnotationPresent(DefineCommand.class)) {
                Command command = new Command(method, method.getAnnotation(DefineCommand.class));
                Commands.put(command.GetIdentifier(), command);
            }
        }
    }

    private void DefineMe(){
        DefineModule moduledefinition = (DefineModule) ModuleClass.getAnnotation(DefineModule.class);
        identifier = moduledefinition.Identifier();
        information = moduledefinition.Information();
    }
}

