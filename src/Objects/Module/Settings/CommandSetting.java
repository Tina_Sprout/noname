package Objects.Module.Settings;

import Utils.Serializers.CommandSettingDe;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CommandSetting {
    @JsonProperty(value = "enabled", required = false)
    public boolean enabled = true;
}
