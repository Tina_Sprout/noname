package Objects.Module.Settings;

import Objects.Module.Command;
import Utils.Serializers.CommandSettingDe;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.HashMap;

public class ModuleSetting {
    @JsonProperty(value = "Enabled", required = false)
    public boolean Enabled = true;
    @JsonProperty(value = "Listeners", required = false) HashMap<String, ListenerSetting> ListenerSettings;
    @JsonProperty(value = "Commands", required = false)
    @JsonDeserialize(using = CommandSettingDe.class)
    public HashMap<String, CommandSetting> Commands= new HashMap<>();
    @JsonProperty(value = "Topics", required =  false) HashMap<String, TopicSetting> Listeners;
  //  @JsonProperty("sadasd") String shit;
//    @JsonProperty("sdadasdasda") int meme;



}
