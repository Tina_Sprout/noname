package Networking.Rest;

import Utils.Refs;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.geometry.Pos;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.logging.Logger;

public class Discord {
    static Logger logger = Logger.getLogger("Rest-Discord");
    static String API = "https://discordapp.com/api/v7/";
    static HashMap<String, String> Headers = new HashMap(){{
       put("User-Agent", Refs.Build);
       put("Content-Type", "Application/json");
       put("Authorization", Refs.Discord_Token);
    }};


    // Base requests
    public static JSONObject Post(String endpoint, JSONObject payload) {
        try {
            return Unirest.post(API+endpoint).headers(Headers).body(payload).asJson().getBody().getObject();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        logger.warning(String.format("Post rest to %s failed with payload: %s",endpoint,payload));
        return null;
    }
    public static JSONObject Get(String endpoint) {
        try {
            return Unirest.get(API+endpoint).headers(Headers).asJson().getBody().getObject();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        logger.warning("Error sending get rest: "+endpoint);
        return null;
    }
    public static JSONArray GetArray(String enpoint) {
        try {
            return Unirest.get(API+enpoint).headers(Headers).asJson().getBody().getArray();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static JSONObject Patch(String endpoint, JSONObject payload) {
        try {
            return Unirest.patch(API+endpoint).headers(Headers).body(payload).asJson().getBody().getObject();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        //todo: discord likes to return eresponse codes instead of json on these so i might wanna add suport for that instead of taking everything as an error
        return null;
    }
    public static JSONObject Delete(String endpoint, JSONObject payload) {
        try {
            return Unirest.delete(API+endpoint).headers(Headers).body(payload).asJson().getBody().getObject();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Channel requests
    public static JSONObject GetChannel(String channelid) {
        return Get("channels/"+channelid);
    }
    public static JSONObject ModifyChannel(String channelid, JSONObject payload) {
        return Patch(channelid,payload);
    }
    public static void DeleteChannel(String channelid) {
        Delete(channelid, new JSONObject());
    }

    public static JSONObject GetGuild(String guildSnowflake) {
        return Get("guilds/"+guildSnowflake);
    }

    //Message requests
    enum SearchBy{around,before,after}
    public static JSONArray GetMessages(String channelid, SearchBy searchBy, String id, int... limit) {
        String limits = (limit != null && limit.length > 0) ? "&limit="+limit : "";
        return GetArray(String.format("channels/%s/messages?%s=%s%s",channelid,searchBy.name(),id,limits));
    }
    public static JSONObject GetMessage(String messageid, String channelid) {
        return Get(String.format("channels/%s/messages/%s",channelid,messageid));
    }
    public static JSONObject CreateMessage(String channelid, JSONObject body) {
        return Post(String.format("channels/%s/messages",channelid),body);
    }
    public static JSONObject CreateMessage(String channelid, String content) {
        return CreateMessage(channelid, new JSONObject().put("content", content));
    }
    public static JSONObject EditMessage(String channelid, String messageid, JSONObject body) {
        return Patch(String.format("channels/%s/messages/%s",channelid,messageid),body);
    }
    public static JSONObject EditMessage(String channelid, String messageid, String content) {
        return EditMessage(channelid,messageid,new JSONObject().put("content", content));
    }
    public static void DeleteMessage(String channelid, String messageid) {
        Delete(String.format("/channels/%s/messages/%s",channelid,messageid), new JSONObject());
    }
    public static void DeleteMessages(String channelid, String[] messageids) {
        Post(String.format("/channels/%s/messages/bulk-delete", channelid), new JSONObject().put("messages", new JSONArray(messageids)));
    }

    //Reactions
    //todo: think of a nice way to encode emojis first

    //Invites
    public static JSONArray GetAllinvites(String channelid) {
        return GetArray(String.format("channels/%s/invites",channelid));
    }
    public static JSONObject CreateInvite(String channelid, JSONObject params) {
        return Post(String.format("/channels/%s/invites",channelid),params);
    }

}
