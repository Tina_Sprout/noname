package Networking.Sockets;

import Utils.Managers.ChannelManager;
import Utils.Managers.SocketManager;
import Utils.Refs;
import com.neovisionaries.ws.client.*;
import jdk.net.Sockets;
import org.json.JSONObject;
import sun.security.provider.SHA;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class GatewaySocket extends WebSocketAdapter {
    int Sequence, ShardID = 0;
    long HeartBeatInterval, CreationDate = 0;
    enum GatewayStatus{NEWLY_CREATED, CONNECTING,CONNECTED,RECONNECTING,DISCONNECTING,DISCONNECTED,ERRORING, NOTCONNECTED}
    GatewayStatus SocketStatus;
    WebSocket webSocket;
    Logger SocketLogger;

    public GatewaySocket(int ShardID) throws IOException {
        this.ShardID = ShardID;
        webSocket = new WebSocketFactory().createSocket(SocketManager.GatewayURL+"?encoding=json&v=7").addListener(this);
        SocketLogger = Logger.getLogger(String.format("Websocket #%s gateway log", ShardID));
        SocketStatus = GatewayStatus.NEWLY_CREATED;
    }

    public void Connect() {
        if(SocketStatus == GatewayStatus.CONNECTED || SocketStatus == GatewayStatus.ERRORING) {
            SocketLogger.warning(String.format("Shard #%s[%s] is already connected to the websocket! Reconnecting instead",ShardID, SocketStatus));
            Disconnect();
            SocketStatus = GatewayStatus.RECONNECTING;
        }
        try {
            SocketLogger.warning(String.format("Shard #%s[%s] is connecting to the gateway",ShardID,SocketStatus));
            SocketStatus = GatewayStatus.CONNECTING;
            webSocket.connect();
            SocketStatus = GatewayStatus.CONNECTED;
            SocketLogger.finest(String.format("Shard #%s[%s] connected to the gateway... authing..",ShardID,SocketStatus));
        } catch (WebSocketException e) {
e.printStackTrace();
            SocketLogger.warning(e.getError().toString()+" In Shard "+ShardID);
        }
    }

    private void Disconnect() {
        SocketStatus = GatewayStatus.DISCONNECTING;
        webSocket.disconnect();
        SocketStatus = GatewayStatus.DISCONNECTED;
        SocketLogger.fine(String.format("Shard #%s successfully disconnecred", ShardID));
    }

    @Override
    public void onTextMessage(WebSocket websocket, String text) throws Exception {

        JSONObject fromsock = new JSONObject(text);
        SocketLogger.fine(String.format("[%s] %s",ShardID, fromsock));
        if(fromsock.has("s") && !fromsock.isNull("s")) {
            Sequence = fromsock.getInt("s");
        }
        switch (fromsock.getInt("op")) {
            case 10: {
                SetupHeartBeat(fromsock.getJSONObject("d"));
                break;
            }
            case 7: {
                SocketLogger.fine("We got told to reconnect by the socket");
                Connect();
                break;
            }
            case 9: {
                //Invalid session;
                Disconnect();
                break;
            }
            case 0: {
                fromsock = new JSONObject(fromsock.toString().replaceAll("\"id\":","\"_id\":"));
                switch (fromsock.getString("t")) {
                    case "MESSAGE_CREATE": {
                        ChannelManager.OnMessageCreated(fromsock.getJSONObject("d"));
                        break;
                    }
                }
            }
        }

    }
    Thread Heartbeater;
    private void SetupHeartBeat(JSONObject d) {
        this.Heartbeater = new Thread(()->{
           while (true) {
               Send(1,new JSONObject().put("d",Sequence));
               try {
                   Thread.sleep(d.getLong("heartbeat_interval"));
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }
        });
        Heartbeater.start();
    }

    @Override
    public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
        Send(2, new JSONObject(String.format("{" +
                "    \"token\": \"my_token\"," +
                "    \"properties\": {" +
                "        \"$os\": \"linux\"," +
                "        \"$browser\": \"%s\"," +
                "        \"$device\": \"%s\"" +
                "    }," +
                "    \"compress\": true," +
                "    \"large_threshold\": 250,\"shards\":[%s,%s]}",Refs.Build,Refs.Build, ShardID, SocketManager.GatewayShards.size())).put("token", Refs.Discord_Token.replaceFirst("bot ","")));
        SocketLogger.finest(String.format("Shard #%s[%s]: Authentificated to websocket",ShardID, SocketStatus));
    }

    public void Send(int i, JSONObject jsonObject) {
        String payload = String.format("{\"op\":%s,\"d\":%s}",i,jsonObject);
        SocketLogger.finest(String.format("Shard #%s[%s]: Sent payload: %s", ShardID,SocketStatus,payload));
        webSocket.sendText(payload);
    }

    @Override
    public void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) throws Exception {
            System.out.println(serverCloseFrame);
            System.exit(0);
    }

}
